import { Component, ViewChild } from '@angular/core';
import { ViewController, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import moment from 'moment'; //using moment for date queries

/**
 * Generated class for the SchedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
})
export class SchedulePage {
  chartData = [];
  labels = [];
  @ViewChild('eventChart') canvas;
  chart: any;

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    let data = navParams.get("data");
    for (let i = 0; i < data.length; i++) { //cant use foreach, seems to break it
      //loop through data given and get difference between startDate and endDate in minutes
      //push this difference value to chartData
      this.chartData.push(moment(data[i].endDate).diff(moment(data[i].startDate), "minutes"));
      this.labels.push(data[i].title);

      //DEBUGGING
      //console.log(data[i].startDate);
      //console.log(moment(data[i].startDate, moment.ISO_8601));
      //console.log(moment.duration(moment(data[i].endDate).diff(moment(data[i].startDate))));
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SchedulePage');
    console.log(this.chartData);
    this.chart = new Chart(this.canvas.nativeElement, {
      type: 'bar',
      data: {
        labels: this.labels,
        datasets: [{
          label: "Event Duration (minutes)",
          backgroundColor: "red",
          data: this.chartData,
        }]
      },
      options: null
    });
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
