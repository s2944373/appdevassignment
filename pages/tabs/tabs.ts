import { Component } from '@angular/core';

import { CalendarPage } from '../calendar/calendar';
import { AddEventPage } from '../add-event/add-event';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AddEventPage;
  tab3Root = CalendarPage;

  constructor() {

  }
}
