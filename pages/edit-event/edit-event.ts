import { Component } from '@angular/core';
import { AlertController, ViewController, NavParams } from 'ionic-angular';
import moment from 'moment';

/**
 * Generated class for the EditEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-edit-event',
  templateUrl: 'edit-event.html',
})
export class EditEventPage {
  eventData = {
    title: '',
    desc: null,
    startDate: null,
    endDate: null,
    type: null,
    photo: null
  }

  constructor(public viewCtrl: ViewController, public navParams: NavParams,
    public alertCtrl: AlertController) {
    this.eventData = navParams.get("data");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditEventPage');
  }

  createAlert(title, sub) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: sub,
      buttons: ['OK']
    });
    alert.present();
  }

  //Update the event and pass back the event data
  updateEvent() {
    if (!moment(this.eventData.startDate, moment.ISO_8601).isBefore(moment(this.eventData.endDate, moment.ISO_8601))) {
      this.createAlert('Invalid End Date', 'Your Start Date must be before your End Date');
    }
    else if (this.eventData.title === '') { this.createAlert('No Title Entered', 'Event Must Have a Title'); }
    else {
      this.viewCtrl.dismiss(this.eventData);
    }
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  imageSelected(files) {
    let fileReader = new FileReader();
    fileReader.onload = e => {
      this.eventData.photo = fileReader.result;
    };
    fileReader.readAsDataURL(files[0]);
  }
}
