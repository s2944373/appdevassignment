import { Component } from '@angular/core';
import { ModalController, NavController, Item } from 'ionic-angular';
import { EditEventPage } from '../edit-event/edit-event';
import { SchedulePage } from '../schedule/schedule';
import { SplashPage } from '../splash/splash';
import { Storage } from '@ionic/storage';
import moment from 'moment'; //using moment for date queries

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  allEvents = [];

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, private storage: Storage) {

    //Push to splash screen
    this.navCtrl.push(SplashPage);

  }

  ionViewDidEnter() {
    //reload events on enter
    this.storage.get("events").then((val) => {
      if (val == null) {
        console.log("no events");
      }
      else {
        // load events and sort them based on start dates
        this.allEvents = val;
        this.allEvents.sort((a, b) => { return <any>moment(a.startDate, moment.ISO_8601).isAfter(moment(b.startDate, moment.ISO_8601)) })

        //DEBUGGING
        //console.log(moment().endOf('isoWeek'));
        //console.log(moment().endOf('month'));
        //console.log(val);
      }
    });
  }

  //check date functions returns true if date is within range
  checkDateToday(i) {
    if (moment(this.allEvents[i].startDate, moment.ISO_8601).isSame(moment(), 'day')) { return true; }
  }
  checkDateTomorrow(i) {
    if (moment(this.allEvents[i].startDate, moment.ISO_8601).isBetween(moment(), moment().add(1, 'days'), 'day', "(]")) { return true; }
  }
  checkDateWeek(i) {
    if (moment(this.allEvents[i].startDate, moment.ISO_8601).isBetween(moment().add(1, 'days'), moment().endOf('isoWeek'), null, "(]")) { return true; }
  }
  checkDateMonth(i) {
    if (moment(this.allEvents[i].startDate, moment.ISO_8601).isBetween(moment().endOf('isoWeek'), moment().endOf('month'), null, "(]")) { return true; }
  }
  checkDateYear(i) {
    if (moment(this.allEvents[i].startDate, moment.ISO_8601).isBetween(moment().endOf('month'), moment().endOf('year'), null, "(]")) { return true; }
  }

  //function to create the Edit Event Page modal
  toUpdate(i) {
    let index = i;
    let updateModal = this.modalCtrl.create(EditEventPage, {
      data: this.allEvents[i]
    });
    updateModal.onDidDismiss(data => {
      //if data was sent back, update event and update view, else do nothing
      if (data) { 
        this.allEvents[index] = data;
        this.storage.set("events", this.allEvents);
      }
    });
    updateModal.present();
  }
  
  //function to pop the Schedule Page
  toSchedule(setting) {
    let scheduleData = [];
    //when navigating to schedule page, need to send in right data to be displayed
    //e.g.todays data, tomorrow data etc.
    switch (setting) {
      case 'today':
        for (let i = 0; i < this.allEvents.length; i++) {
          if (this.checkDateToday(i)) scheduleData.push(this.allEvents[i]);
        }
        break;
      case 'tomorrow':
        for (let i = 0; i < this.allEvents.length; i++) {
          if (this.checkDateTomorrow(i)) scheduleData.push(this.allEvents[i]);
        }
        break;
      case 'week':
        for (let i = 0; i < this.allEvents.length; i++) {
          if (this.checkDateWeek(i)) scheduleData.push(this.allEvents[i]);
        }
        break;
      case 'month':
        for (let i = 0; i < this.allEvents.length; i++) {
          if (this.checkDateMonth(i)) scheduleData.push(this.allEvents[i]);
        }
        break;
      case 'year':
        for (let i = 0; i < this.allEvents.length; i++) {
          if (this.checkDateYear(i)) scheduleData.push(this.allEvents[i]);
        }
        break;
    }
    let scdeduleModal = this.modalCtrl.create(SchedulePage, { data: scheduleData});
    scdeduleModal.present();
  }

  delete(i) { // delet this
    this.allEvents.splice(i, 1);
    this.storage.set("events", this.allEvents);
  }

  //Filters (search) items by comparing the title
  filterItems(ev: any) {
    let val = ev.target.value;

    if (val && val.trim() !== '') {
      this.allEvents = this.allEvents.filter(function(allEvents) {
        return allEvents.title.toLowerCase().includes(val.toLowerCase());
        //console.log(allEvents.title);
      });
    }
  }
}
