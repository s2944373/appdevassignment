import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import moment from 'moment';

/**
 * Generated class for the AddEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-event',
  templateUrl: 'add-event.html',
})
export class AddEventPage {
  currenDate = moment();
  allEvents = null;
  eventData = {
    title: '',
    desc: null,
    startDate: new Date().toISOString(),
    endDate: null,
    type: null,
    photo: null
  }
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,
    public alertCtrl: AlertController) {
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter AddEventPage');
    // on enter get events
    this.storage.get("events").then((val) => {
      if (val == null) {
        this.allEvents = [];
      }
      else {
        this.allEvents = val;
      }
    });
  }

  createAlert(title, sub) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: sub,
      buttons: ['OK']
    });
    alert.present();
  }

  addEvent() {
    console.log(this.eventData);
    //preform data validation checks first
    //add event and redirect to home page
    if (!moment(this.eventData.startDate, moment.ISO_8601).isBefore(moment(this.eventData.endDate, moment.ISO_8601))) {
      this.createAlert('Invalid End Date', 'Your Start Date must be before your End Date');
    }
    else if (this.eventData.title === '') { this.createAlert('No Title Entered', 'Event Must Have a Title');}
    else {
      this.allEvents.push(this.eventData);
      this.storage.set("events", this.allEvents);
      this.navCtrl.parent.select(0);
    }
  }

  //image upload
  imageSelected(files) {
    let fileReader = new FileReader();
    fileReader.onload = e => {
      this.eventData.photo = fileReader.result;
    };
    fileReader.readAsDataURL(files[0]);
  }
}
